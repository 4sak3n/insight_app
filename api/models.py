from django.contrib.postgres.search import SearchVectorField
from django.contrib.postgres.indexes import GinIndex
from django.db import models

class EntryOrigin(models.Model):
    oid = models.AutoField(primary_key=True)
    path = models.CharField(unique=True, max_length=255)

class EntryUsage(models.Model):
    oid = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=255)

class EntryCategory(models.Model):
    cid = models.AutoField(primary_key=True)
    path = models.CharField(unique=True, max_length=255)

class Entry(models.Model):
    id = models.CharField(max_length=10, primary_key=True, db_index=True)
    headword = models.CharField(max_length=255, db_index=True)
    headword_slug = models.SlugField(max_length=255, allow_unicode=True, db_index=True)
    part_of_speech = models.CharField(max_length=255)
    quotations = models.TextField()
    hw_sort_key = models.CharField(max_length=255)
    ps_sort_key = models.CharField(max_length=255)
    fu_sort_key = models.FloatField(default=9999.9)
    first_use = models.PositiveSmallIntegerField()
    list_data = models.TextField()
    cite_data = models.TextField()
    categories = models.ManyToManyField(EntryCategory)
    usages = models.ManyToManyField(EntryUsage)
    origins = models.ManyToManyField(EntryOrigin)

    def __str__(self):
        return "{0} ({1})".format(self.headword, self.id)

