# Generated by Django 2.1.7 on 2019-07-11 17:28

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Entry',
            fields=[
                ('id', models.CharField(db_index=True, max_length=10, primary_key=True, serialize=False)),
                ('headword', models.CharField(db_index=True, max_length=255)),
                ('headword_slug', models.SlugField(allow_unicode=True, max_length=255)),
                ('part_of_speech', models.CharField(max_length=255)),
                ('quotations', models.TextField()),
                ('hw_sort_key', models.CharField(max_length=255)),
                ('ps_sort_key', models.CharField(max_length=255)),
                ('fu_sort_key', models.FloatField(default=9999.9)),
                ('first_use', models.PositiveSmallIntegerField()),
                ('list_data', models.TextField()),
                ('cite_data', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='EntryCategory',
            fields=[
                ('cid', models.AutoField(primary_key=True, serialize=False)),
                ('path', models.CharField(max_length=255, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='EntryOrigin',
            fields=[
                ('oid', models.AutoField(primary_key=True, serialize=False)),
                ('path', models.CharField(max_length=255, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='EntryUsage',
            fields=[
                ('oid', models.AutoField(primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=255, unique=True)),
            ],
        ),
        migrations.AddField(
            model_name='entry',
            name='categories',
            field=models.ManyToManyField(to='api.EntryCategory'),
        ),
        migrations.AddField(
            model_name='entry',
            name='origins',
            field=models.ManyToManyField(to='api.EntryOrigin'),
        ),
        migrations.AddField(
            model_name='entry',
            name='usages',
            field=models.ManyToManyField(to='api.EntryUsage'),
        ),
    ]
