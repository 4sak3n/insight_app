from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from carousel.models import Slide
from selenium import webdriver
import requests


class TestCarousel(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--headless')
        cls.browser = webdriver.Chrome(chrome_options=chrome_options)

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test_img_resolve(self):
        # find all img elements on page
        self.browser.get('https://www.google.com')
        imgs = self.browser.find_elements_by_tag_name('img')

        for img in imgs:
            with self.subTest(img=img):
                # make a GET request for just the img
                url = img.get_attribute('src')
                resp = requests.get(url)

                # check the request returns 200 status code
                self.assertEqual(resp.status_code, 200)

    # extra credit test
    def test_img_metadata(self):
        self.browser.get('https://www.google.com')
        imgs = self.browser.find_elements_by_tag_name('img')

        for img in imgs:
            with self.subTest(img=img):
                # check to see whether slide exists in db
                # https://docs.djangoproject.com/en/2.1/topics/db/models/
                url = img.get_attribute('src')
                slides = Slide.objects.filter(image=url)

                if slides:
                    print(slides[0].image_reference)
                else:
                    print("Slide not in db.")