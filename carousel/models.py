from django.utils.encoding import python_2_unicode_compatible
from api.models import Entry
from django.db import models


# helper function to transform image path and set name as <entry_id>.<file_extension>
def slide_img_file_path(instance, filename):
    file_ext = filename.split('.')[-1]
    return 'static/dsae/img/slides/{0}.{1}'.format(instance.entry.id, file_ext)

# SLide model for the home page slideshow
@python_2_unicode_compatible
class Slide(models.Model):
    entry = models.OneToOneField(Entry, on_delete=models.PROTECT, primary_key=True)
    image = models.ImageField(upload_to=slide_img_file_path)
    image_reference = models.TextField(null=True, blank=True)
    legal_reference = models.TextField(null=True, blank=True)
    user_reference = models.TextField(null=True, blank=True)

    class Meta:
        ordering = ["entry__hw_sort_key","entry__ps_sort_key"]

    def __str__(self):
        return '{0} ({1})'.format(self.entry.headword, self.entry.id)

